package com.an1sht.msscbeerservice.web.model;

public enum BeerStyleEnum {
    LAGER, PILSNER, STOUT, GOSE, PORTER, ALE, WHEAT, IPA, PALE_ALE, SAISON
}
